# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from datetime import datetime, timezone

from zipline.aapi import record, symbol, order
from zipline import run_algorithm

import pandas as pd

def initialize (context):
    pass

def handle_data (context, data):
    order (symbol('APPL'), 10)
    record (AAPL = data.current(symbol('APPL'), 'price'))
    
start = datetime (2016, 3, 1, tzinfo=timezone.utc)
end = datetime (216, 4, 1, tzinfo=timezone.utc)
start = pd.Timestamp(start, tz='UTC', offset='C')
end = pd.Timestamp(end, tz='UTC', offset='C')

results = run_algorithm(
        start=start,
        end=end,
        initialize=initialize,
        handle_data=handle_data,
        capital_base=10000,
        data_frequency='daily')

